package com.hibernate.modelo;

import javax.persistence.*;


@Entity
@Table(name="Modulo", uniqueConstraints=@UniqueConstraint(columnNames= {"ID"}))
public class Modulo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nombre;
	private float creditos;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getCreditos() {
		return creditos;
	}
	public void setCreditos(float creditos) {
		this.creditos = creditos;
	}

}
