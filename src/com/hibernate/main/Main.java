package com.hibernate.main;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


import org.hibernate.Session;
import org.hibernate.query.Query;

import com.hibernate.modelo.Correo;
import com.hibernate.modelo.Direccion;
import com.hibernate.modelo.Modulo;
import com.hibernate.modelo.Profesor;
import com.hibernate.utilidades.Utilidades;

public class Main {

	public static void main(String[] args) {
					
					//conectamos y iniciamos la bd
					Session session = Utilidades.getSessionFactory().openSession();
					
					
						
						do {
							
							Scanner opciones = new Scanner(System.in);
							String opcionElegida1;
							int opcionElegida2;
							float opcionElegida3;

							System.out.println("1 Listar");
							System.out.println("2 Agregar");
							System.out.println("3 Actualizar");
							System.out.println("4 Borrar");
							System.out.println("5 Salir del programa");
							System.out.print(">> ");
		    				opcionElegida1 = opciones.next();
				    		System.out.println();
				    		
				    		switch(opcionElegida1) {
				    		
				    		case "1": 		
				    			
				    			session.beginTransaction();
				    			
				    			//codigo para listar toda la tabla
				    			Query query = session.createQuery("from Profesor");
				    			List lista = query.list();
				    		
				    			for( Object p : lista) {
				    			
				    				Profesor p2 = (Profesor) p;
				    				System.out.println(p2.getId()+" "+p2.getNombre()+" "+p2.getApe1()+" "+p2.getApe2()
				    				+" "+p2.getCorreos().toString()+" "+
				    				p2.getDireccion().getCalle()+" "+p2.getDireccion().getNumero()+" "+p2.getDireccion().getPoblacion()+" "+p2.getDireccion().getProvincia()+" "+
				    				p2.getModulo().getNombre()+" "+p2.getModulo().getCreditos());
				    			
				    			}
				    			
				    				session.getTransaction().commit();

				    				System.out.println();
				    				break;
				    		
				    		case "2": 		
				    			
				    				session.beginTransaction();
				    			
				    				List<Correo> correos =  new ArrayList<Correo>();
				    			
				    				Profesor p1 = new Profesor();
				    			
				    				System.out.println("Escoge un nombre: ");
				    				opcionElegida1 = opciones.next();
				    				p1.setNombre(opcionElegida1);
			
				    				System.out.println("Escoge el primer apellido: ");
				    				opcionElegida1 = opciones.next();
				    				p1.setApe1(opcionElegida1);
				    			
				    				System.out.println("Escoge el segundo apellido: ");
				    				opcionElegida1 = opciones.next();
				    				p1.setApe2(opcionElegida1);
				    			
				    							    			
				    				System.out.println("Indique el numero de correos del profesor: ");
				    				opcionElegida2 = opciones.nextInt();	
				    			
				    			for(int i = 0;i<opcionElegida2;i++) {
				    				
				    				Correo cor = new Correo();
					    			System.out.println("Escoge la direccion: ");
					    			opcionElegida1 = opciones.next();
					    			cor.setDireccion(opcionElegida1);
					    			System.out.println("Escoge el proveedor: ");
					    			opcionElegida1 = opciones.next();
					    			cor.setProveedor(opcionElegida1);
					    			correos.add(cor);
				    				
				    			}

				    			p1.setCorreos(correos);
				    			
				    			Direccion dir = new Direccion();
				    			
				    			System.out.println("Escoge la calle: ");
				    			opcionElegida1 = opciones.next();
				    			dir.setCalle(opcionElegida1);

				    			System.out.println("Escoge el numero: ");
				    			opcionElegida2 = opciones.nextInt();
				    			dir.setNumero(opcionElegida2);

				    			System.out.println("Escoge la poblacion: ");
				    			opcionElegida1 = opciones.next();
				    			dir.setPoblacion(opcionElegida1);

				    			System.out.println("Escoge la provincia: ");
				    			opcionElegida1 = opciones.next();
				    			dir.setProvincia(opcionElegida1);
				    			
				    			p1.setDireccion(dir);
				    			
				    			Modulo modulo = new Modulo();
				    					
					    		System.out.println("Escpge el nombre del modulo: ");
					    		opcionElegida1 = opciones.next();
				    			modulo.setNombre(opcionElegida1);
								System.out.println("Escoge los creditos(debe tener coma): ");
					    		opcionElegida3 = opciones.nextFloat();
				   				modulo.setCreditos(opcionElegida3);
				    					
				    			p1.setModulo(modulo);

				    			session.save(p1);
				    			session.getTransaction().commit();
				    		
				    			System.out.println();
				    		
				    			break;
				    			
				    		case "3":
				    			
				    			session.beginTransaction();
				    			
			    				System.out.println("indique el id que quiere modificar: ");
			    				opcionElegida2 = opciones.nextInt();
				    			
				    			Profesor p2 = (Profesor)session.get(Profesor.class,opcionElegida2);
				    			
				    			List<Correo> correos2 =  new ArrayList<Correo>();
				    			
				    			System.out.println("Escoge un nombre: ");
				    			opcionElegida1 = opciones.next();
				    			p2.setNombre(opcionElegida1);
				    			
				    			System.out.println("Escoge el primer apellido: ");
				    			opcionElegida1 = opciones.next();
				    			p2.setApe1(opcionElegida1);
				    			
				    			System.out.println("Escoge el segundo apellido: ");
				    			opcionElegida1 = opciones.next();
				    			p2.setApe2(opcionElegida1);
				    			
				    							    			
				  				System.out.println("Indique cuantos correos tendra el profesor: ");
				   				opcionElegida2 = opciones.nextInt();	
				    			
			    				for(int i = 0;i<opcionElegida2;i++) {
				    				
			    				Correo cor = new Correo();
				    			System.out.println("Escoge la direccion: ");
				    			opcionElegida1 = opciones.next();
				    			cor.setDireccion(opcionElegida1);
					    		System.out.println("escoge el proveedor: ");
					    		opcionElegida1 = opciones.next();
					    		cor.setProveedor(opcionElegida1);
					   			correos2.add(cor);
				    			
				   				}

				    			p2.setCorreos(correos2);
				    			
				   				Direccion dir2 = new Direccion();
				   			
				   				System.out.println("Escoge la calle: ");
			    				opcionElegida1 = opciones.next();
			    				dir2.setCalle(opcionElegida1);

			    				System.out.println("Escoge el numero: ");
			    				opcionElegida2 = opciones.nextInt();
			    				dir2.setNumero(opcionElegida2);

				    			System.out.println("Escoge la poblacion: ");
				    			opcionElegida1 = opciones.next();
				    			dir2.setPoblacion(opcionElegida1);
				    			
				    			System.out.println("Escoge la provincia: ");
				    			opcionElegida1 = opciones.next();
				    			dir2.setProvincia(opcionElegida1);
				    			
				    			p2.setDireccion(dir2);
				    			
				    			Modulo modulo2 = new Modulo();
				    					
					    		System.out.println("Escoge el nombre del modulo: ");
					    		opcionElegida1 = opciones.next();
				    			modulo2.setNombre(opcionElegida1);
					    		System.out.println("Escoge los creditos(deben tener coma): ");
					    		opcionElegida3 = opciones.nextFloat();
				    			modulo2.setCreditos(opcionElegida3);
				    					
				    			p2.setModulo(modulo2);
				    			
				    			session.update(p2);
				    			session.getTransaction().commit();
				    			
				    			System.out.println();
				    			
				    			break;
				    			
				    		case "4":
				    			
				    			session.beginTransaction();
				    			
			    				System.out.println("Indique el ID que quiere eliminar: ");
			    				opcionElegida2 = opciones.nextInt();			    			
				    			Profesor p3 = (Profesor)session.load(Profesor.class,opcionElegida2);
				    			
				    			session.remove(p3);				    			
				    			session.getTransaction().commit();
				    			
				    			System.out.println();
				    			
				    			break;
				    			
				    		case "5": 
				    			
				    			Utilidades.getSessionFactory().close();
				    			System.exit(0);
					    	
				    			default: System.out.println("Escoge una opcion correcta");
				    			System.out.println();
				    		
				    		}
							
						}while(true);
						
					} 

}
